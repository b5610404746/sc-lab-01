

public class Card {
	private int money;
	private String name;

	public Card(int money) {
		this.money = money;
	}
	public void reFill(int amount) {
		this.money += amount;
	}
	public void buy(int amount) {
		this.money -= amount;
	}

	public void setName(String str) {
		name = str;
	}

	public String toString() {
		return money + "  " ;
	}

}