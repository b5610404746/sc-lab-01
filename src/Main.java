

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

public class Main{

	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);
		}
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Main();
	}

	public Main() {
		frame = new Frame();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(400, 400);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
	
		Card C1 = new Card(0); 
		frame.setResult("You have "+C1.toString());		
		C1.reFill(500);
		frame.extendResult("refill " + C1.toString());
		C1.buy(250);
		frame.extendResult("buy " + C1.toString());
		C1.toString();
		frame.extendResult("balance " + C1.toString());
	}

	ActionListener list;
	Frame frame;
}